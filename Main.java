import java.util.Arrays;

    public class Main {
        public static void main(String[] args) {
            int[] array1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            int min = Arrays.stream(array1).min().getAsInt();
            int max = Arrays.stream(array1).max().getAsInt();
            System.out.println(min);
            System.out.println(max);
            System.out.println(min + max + " = min + max");
            System.out.println("Hello, GIT !");
        }
    }


